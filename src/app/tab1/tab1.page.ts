import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonHeader, IonToolbar, IonTitle, IonContent } from '@ionic/angular/standalone';
import * as L from 'leaflet';
import { Observable, max } from 'rxjs';
import 'leaflet.heat';
import { GATEWAYS } from '../../assets/gateways';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  standalone: true,
  imports: [IonHeader, IonToolbar, IonTitle, IonContent],
})
export class Tab1Page {

  map: any;
  gateways = GATEWAYS.gateways;
  layerControl: L.Control.Layers = L.control.layers();
  gatewayLayersControl: L.Control.Layers = L.control.layers({}, {}, {collapsed: false, position: 'bottomright'});
  minZomm: number = 13;
  maxZoom: number = 18;
  gatewayData: any = [];
  
  constructor(public httpClient: HttpClient) {}
  
  ionViewDidEnter(){
    // gradient for heatmap colors
    let gradient = {0.52: 'blue', 0.64: 'lime', .76: 'yellow', 0.88: 'orange', 1.0: 'red'};

    // initialize map
    this.map = new L.Map('map', {
      minZoom: this.minZomm,
      maxZoom: this.maxZoom,
      maxBounds: L.latLngBounds([51.074625519670136, 7.976074218750001], [50.981668164844045, 7.72441864013672]),
      center: [51.0282, 7.8503],
      zoom: 15
    });

    this.layerControl.addTo(this.map);
    this.gatewayLayersControl.addTo(this.map);
    
    this.initBasemap();
    this.initGateways();


    let heatLayer: L.HeatLayer;
    
    // GET request to backend
    let measurments:Observable<any> = this.httpClient.get('https://express.darmstadt.beispiel-stadt.de', {responseType: 'json'});

    // handle data from GET request
    measurments.subscribe( data => {
      for (var gateway in this.gateways){
        this.gatewayData[this.gateways[gateway].id] = {
            'name': this.gateways[gateway].name,
            'data': []
        };
      }

      heatLayer = L.heatLayer([], {radius: 35,  gradient: gradient});

      for(var row in data){
        data[row][0][2] = data[row][0][2]/250;
        this.gatewayData['eui-' + data[row][1].toLowerCase()].data.push(data[row][0]);
        heatLayer.addLatLng(data[row][0]);
      }
      this.gatewayLayersControl.addBaseLayer(heatLayer, "Alle");
      for (var gateway in this.gatewayData){
        this.gatewayLayersControl.addBaseLayer(L.heatLayer( this.gatewayData[gateway].data, {radius: 35, gradient: gradient}), this.gatewayData[gateway].name);
      }
      heatLayer.addTo(this.map);
    });
    this.map.invalidateSize();
  }

  /**
   * Add base layers to map
   */
  initBasemap(){
    var osm = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    this.layerControl.addBaseLayer(osm, 'OSM');

    var OpenStreetMap_HOT = L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Tiles style by <a href="https://www.hotosm.org/" target="_blank">Humanitarian OpenStreetMap Team</a> hosted by <a href="https://openstreetmap.fr/" target="_blank">OpenStreetMap France</a>'
    });
    this.layerControl.addBaseLayer(OpenStreetMap_HOT, 'OSM HOT');

    var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
    });
    this.layerControl.addBaseLayer(Esri_WorldImagery, 'ESRI');

    osm.addTo(this.map);
  }

  /**
   * Add layer with marker for all gateways to map
   */
  initGateways(){
    var gatewayIcon = new L.Icon({
      iconUrl: "../../assets/gateway.svg",
      iconSize:     [38, 95],
      shadowSize:   [50, 64],
      iconAnchor:   [22, 94],
      shadowAnchor: [4, 62],
      popupAnchor:  [-3, -76]
    });
    var gatewayLayer = L.layerGroup();
    for (var gateway in this.gateways) {
      gatewayLayer.addLayer(L.marker([this.gateways[gateway].lat, this.gateways[gateway].lng], {icon: gatewayIcon}).bindPopup(this.gateways[gateway].name));
    }
    gatewayLayer.addTo(this.map);
    this.layerControl.addOverlay(gatewayLayer, "Gateways");
  }
}
