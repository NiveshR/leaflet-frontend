FROM node:21-slim
WORKDIR /app
COPY package*.json /app/
RUN npm install -g @angular/cli
RUN npm cache clean --force
COPY ./ /app/
RUN npm install
RUN npm cache clean --force
EXPOSE 8100
# ENTRYPOINT ["ng"]
RUN ulimit -n 99999
CMD ng serve --port=8100 --host=0.0.0.0 --live-reload=false --poll=4000 --disable-host-check
